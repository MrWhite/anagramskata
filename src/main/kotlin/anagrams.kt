import java.util.ArrayList

class anagrams {
    fun eval(s: String): String {
        val solution= eval2List(s).joinToString(" ")
        println(solution)
        return solution
    }

    fun eval2List(s: String) : List<String> {
        return when (s.length) {
            1 -> listOf(s)
            2 -> listOf(s, "${s[1]}${s[0]}")
            else -> {
                val result = ArrayList<String>()
                for (position: Int in 0..s.lastIndex) {
                    val response =StringBuilder(s.substring(0, position) + s.substring(position+1, s.length)).toString()
                    val temporaryResult= this.eval2List(response).map {cadena -> StringBuilder(s[position]+cadena).toString()}.toList()
                    result.addAll(temporaryResult)
                }
                return result
            }
        }
    }
}

