import org.junit.jupiter.api.Test

class anagramsTest {
    private val anagrams = anagrams()
    @Test
    fun anagramsSingleLetter() {


        assert(anagrams.eval("j").equals("j"))

    }
    @Test
    fun anagramsTwoLetter() {


        assert(anagrams.eval("ja").equals("ja aj"))

    }
    @Test
    fun anagramsTwoLetterB() {


        assert(anagrams.eval("le").equals("le el"))

    }

    @Test
    fun anagramsTwoLetterC() {


        assert(anagrams.eval("lk").equals("lk kl"))

    }

    @Test
    fun anagramsThreeLetter() {

        assert(anagrams.eval("bir").equals("bir bri ibr irb rbi rib"))

    }

    @Test
    fun anagramsThreeLetterB() {

        assert(anagrams.eval("sdo").equals("sdo sod dso dos osd ods"))

    }
    @Test
    fun anagramsThreeLetterC() {

        assert(anagrams.eval("klñ").equals("klñ kñl lkñ lñk ñkl ñlk"))

    }

    @Test
    fun anagramsFourLetter() {

        assert(anagrams.eval("biro").equals("biro bior brio broi boir bori ibro ibor irbo irob iobr iorb rbio rboi ribo riob robi roib obir obri oibr oirb orbi orib"))


    }

    @Test
    fun anagramsJavierLetter() {

        assert(anagrams.eval("javier").equals("javier javire javeir javeri javrie javrei jaiver jaivre jaievr jaierv jairve jairev jaevir jaevri jaeivr jaeirv jaervi jaeriv jarvie jarvei jarive jariev jarevi jareiv jvaier jvaire jvaeir jvaeri jvarie jvarei jviaer jviare jviear jviera jvirae jvirea jveair jveari jveiar jveira jverai jveria jvraie jvraei jvriae jvriea jvreai jvreia jiaver jiavre jiaevr jiaerv jiarve jiarev jivaer jivare jivear jivera jivrae jivrea jieavr jiearv jievar jievra jierav jierva jirave jiraev jirvae jirvea jireav jireva jeavir jeavri jeaivr jeairv jearvi jeariv jevair jevari jeviar jevira jevrai jevria jeiavr jeiarv jeivar jeivra jeirav jeirva jeravi jeraiv jervai jervia jeriav jeriva jravie jravei jraive jraiev jraevi jraeiv jrvaie jrvaei jrviae jrviea jrveai jrveia jriave jriaev jrivae jrivea jrieav jrieva jreavi jreaiv jrevai jrevia jreiav jreiva ajvier ajvire ajveir ajveri ajvrie ajvrei ajiver ajivre ajievr ajierv ajirve ajirev ajevir ajevri ajeivr ajeirv ajervi ajeriv ajrvie ajrvei ajrive ajriev ajrevi ajreiv avjier avjire avjeir avjeri avjrie avjrei avijer avijre aviejr avierj avirje avirej avejir avejri aveijr aveirj averji averij avrjie avrjei avrije avriej avreji avreij aijver aijvre aijevr aijerv aijrve aijrev aivjer aivjre aivejr aiverj aivrje aivrej aiejvr aiejrv aievjr aievrj aierjv aiervj airjve airjev airvje airvej airejv airevj aejvir aejvri aejivr aejirv aejrvi aejriv aevjir aevjri aevijr aevirj aevrji aevrij aeijvr aeijrv aeivjr aeivrj aeirjv aeirvj aerjvi aerjiv aervji aervij aerijv aerivj arjvie arjvei arjive arjiev arjevi arjeiv arvjie arvjei arvije arviej arveji arveij arijve arijev arivje arivej ariejv arievj arejvi arejiv arevji arevij areijv areivj vjaier vjaire vjaeir vjaeri vjarie vjarei vjiaer vjiare vjiear vjiera vjirae vjirea vjeair vjeari vjeiar vjeira vjerai vjeria vjraie vjraei vjriae vjriea vjreai vjreia vajier vajire vajeir vajeri vajrie vajrei vaijer vaijre vaiejr vaierj vairje vairej vaejir vaejri vaeijr vaeirj vaerji vaerij varjie varjei varije variej vareji vareij vijaer vijare vijear vijera vijrae vijrea viajer viajre viaejr viaerj viarje viarej viejar viejra vieajr viearj vierja vieraj virjae virjea viraje viraej vireja vireaj vejair vejari vejiar vejira vejrai vejria veajir veajri veaijr veairj vearji vearij veijar veijra veiajr veiarj veirja veiraj verjai verjia veraji veraij verija veriaj vrjaie vrjaei vrjiae vrjiea vrjeai vrjeia vrajie vrajei vraije vraiej vraeji vraeij vrijae vrijea vriaje vriaej vrieja vrieaj vrejai vrejia vreaji vreaij vreija vreiaj ijaver ijavre ijaevr ijaerv ijarve ijarev ijvaer ijvare ijvear ijvera ijvrae ijvrea ijeavr ijearv ijevar ijevra ijerav ijerva ijrave ijraev ijrvae ijrvea ijreav ijreva iajver iajvre iajevr iajerv iajrve iajrev iavjer iavjre iavejr iaverj iavrje iavrej iaejvr iaejrv iaevjr iaevrj iaerjv iaervj iarjve iarjev iarvje iarvej iarejv iarevj ivjaer ivjare ivjear ivjera ivjrae ivjrea ivajer ivajre ivaejr ivaerj ivarje ivarej ivejar ivejra iveajr ivearj iverja iveraj ivrjae ivrjea ivraje ivraej ivreja ivreaj iejavr iejarv iejvar iejvra iejrav iejrva ieajvr ieajrv ieavjr ieavrj iearjv iearvj ievjar ievjra ievajr ievarj ievrja ievraj ierjav ierjva ierajv ieravj iervja iervaj irjave irjaev irjvae irjvea irjeav irjeva irajve irajev iravje iravej iraejv iraevj irvjae irvjea irvaje irvaej irveja irveaj irejav irejva ireajv ireavj irevja irevaj ejavir ejavri ejaivr ejairv ejarvi ejariv ejvair ejvari ejviar ejvira ejvrai ejvria ejiavr ejiarv ejivar ejivra ejirav ejirva ejravi ejraiv ejrvai ejrvia ejriav ejriva eajvir eajvri eajivr eajirv eajrvi eajriv eavjir eavjri eavijr eavirj eavrji eavrij eaijvr eaijrv eaivjr eaivrj eairjv eairvj earjvi earjiv earvji earvij earijv earivj evjair evjari evjiar evjira evjrai evjria evajir evajri evaijr evairj evarji evarij evijar evijra eviajr eviarj evirja eviraj evrjai evrjia evraji evraij evrija evriaj eijavr eijarv eijvar eijvra eijrav eijrva eiajvr eiajrv eiavjr eiavrj eiarjv eiarvj eivjar eivjra eivajr eivarj eivrja eivraj eirjav eirjva eirajv eiravj eirvja eirvaj erjavi erjaiv erjvai erjvia erjiav erjiva erajvi erajiv eravji eravij eraijv eraivj ervjai ervjia ervaji ervaij ervija erviaj erijav erijva eriajv eriavj erivja erivaj rjavie rjavei rjaive rjaiev rjaevi rjaeiv rjvaie rjvaei rjviae rjviea rjveai rjveia rjiave rjiaev rjivae rjivea rjieav rjieva rjeavi rjeaiv rjevai rjevia rjeiav rjeiva rajvie rajvei rajive rajiev rajevi rajeiv ravjie ravjei ravije raviej raveji raveij raijve raijev raivje raivej raiejv raievj raejvi raejiv raevji raevij raeijv raeivj rvjaie rvjaei rvjiae rvjiea rvjeai rvjeia rvajie rvajei rvaije rvaiej rvaeji rvaeij rvijae rvijea rviaje rviaej rvieja rvieaj rvejai rvejia rveaji rveaij rveija rveiaj rijave rijaev rijvae rijvea rijeav rijeva riajve riajev riavje riavej riaejv riaevj rivjae rivjea rivaje rivaej riveja riveaj riejav riejva rieajv rieavj rievja rievaj rejavi rejaiv rejvai rejvia rejiav rejiva reajvi reajiv reavji reavij reaijv reaivj revjai revjia revaji revaij revija reviaj reijav reijva reiajv reiavj reivja reivaj"))


    }
}